#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   shadowsockets-libev.py    
@Contact :   sp106168@gmail.com
@License :   (C)Copyright 2014-2019

@Modify Time      @Author    @Version    @Description
------------      -------    --------    -----------
2019/9/29 11:54 AM   moore      1.0         None
"""

# import lib


import os


def clone_shadowsockets():
    if os.path.exists('shadowsocks-libev'):
        autobuild()
    else:
        if os.system('git clone https://github.com/shadowsocks/shadowsocks-libev.git') == 0:
            if os.path.exists('shadowsocks-libev'):
                print('shadowsocks-libev clone success！')
                if os.system('cd shadowsocks-libev && git submodule update --init --recursive') == 0:
                    print('shadowsocks-libev submodule clone success！')
                    autobuild()
                else:
                    print('shadowsocks-libev submodule clone failed！')
            else:
                print('shadowsocks-libev clone failed！')
        else:
            print('shadowsocks-libev clone failed！')

    exit()


def autobuild():
    if os.system('cd shadowsocks-libev && ./autogen.sh') == 0:
        print('autogen success!')
    else:
        print('autogen failed!')


if __name__ == '__main__':
    clone_shadowsockets()

